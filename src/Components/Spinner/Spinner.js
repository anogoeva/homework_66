import React from 'react';
import './Spinner.css';
import Backdrop from "../Backdrop/Backdrop";

const Spinner = (props) => {
    return (
        <>
            <Backdrop
                show={props.show}
                onClick={props.close}
            />
        <div className="Spinner"
             style={{
                 transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
                 opacity: props.show ? '1' : '0',
             }}
        >Loading...</div>
        </>

    )
};

export default Spinner;