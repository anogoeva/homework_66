import React, {useEffect, useState} from 'react';
import axios from "axios";
import axiosApi from "../../axiosApi";

const EditPage = ({history}) => {
    const [allContent, setAllContent] = useState({});
    useEffect(() => {
        const fetchData = async () => {
            const response = await axiosApi.get('.json');
            setAllContent(response.data);
        };
        fetchData().catch(console.error);
    }, []);

    const [type, setType] = useState({});
    const [inputBody, setInputBody] = useState({
        type: '',
        title: '',
        content: '',
        img: ''
    });

    useEffect(() => {
        const fetchData = async () => {
            if (type) {
                const response = await axiosApi.get('/' + type.type + '.json');
                setInputBody(response.data);
            }
        };
        fetchData().catch(console.error);
    }, [type]);

    const editContent = () => {
        const fetchData = async () => {
            axios({
                method: 'put',
                url: 'https://homework-65-685b9-default-rtdb.firebaseio.com/pages/' + type.type + '.json',
                data: {
                    title: inputBody.title,
                    content: inputBody.content,
                    img: inputBody.img
                }
            })
                .then(r => history.replace('/pages/' + type.type));
        };
        fetchData().catch(e => console.error(e));
    };
    const onInputTextareaChange = e => {
        const {name, value} = e.target;
        setInputBody(prev => ({
            ...prev,
            [name]: value
        }));
    };
    const onInputType = e => {
        const {name, value} = e.target;
        setType(prev => ({
            ...prev,
            [name]: value
        }));
    };

    return (
        <div>
            <form action="">
                <select onChange={onInputType} name="type">
                    {Object.keys(allContent).map((option) => (
                        <option key={option} value={option}>{option}</option>))}
                </select>
                <p><label htmlFor="">Title</label></p>
                <input type="text" onChange={onInputTextareaChange} value={inputBody ? inputBody.title : ''}
                       name="title"/>
                <p><label htmlFor="Description" className="AddLabel">Content</label></p>
                <textarea
                    value={inputBody ? inputBody.content : ''}
                    onChange={onInputTextareaChange} cols="55" rows="5"
                    className="form" name="content"/>
                <p><label htmlFor="">Picture</label></p>
                <input type="text" name="img" onChange={onInputTextareaChange} value={inputBody ? inputBody.img : ''}/>
                <br/>
                <button type="button" onClick={editContent}>Save</button>
            </form>
        </div>
    );
};

export default EditPage;