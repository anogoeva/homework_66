import React, {useEffect, useMemo, useState} from 'react';
import Spinner from "../Components/Spinner/Spinner";

const withLoader = (WrappedComponent, axios) => {
    return function ErrorHandlerHOC(props) {
        const [showSpinner, setShowSpinner] = useState(null);

        axios.interceptors.request.use(req => {
            setShowSpinner(true);
            return req;
        });

        axios.interceptors.response.use(res => {
            setShowSpinner(false);
            return res;
        }, err => {
            return err;
        });

        const icId = useMemo(() => {
            return axios.interceptors.response.use(
                null,
                error => {
                    setShowSpinner(error);
                    setShowSpinner(false);
                    throw error;
                }
            );
        }, []);

        useEffect(() => {
            return () => {
                axios.interceptors.response.eject(icId);
            }
        }, [icId]);

        const dismiss = () => {
            setShowSpinner(false);
        }
        return (
            <>
                <WrappedComponent {...props} />
                <Spinner show={Boolean(showSpinner)} close={dismiss}>
                    {showSpinner && showSpinner.message}
                </Spinner>
            </>
        );
    };
};

export default withLoader;